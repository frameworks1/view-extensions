Pod::Spec.new do |s|
  s.name             = 'DPViewExtensions'
  s.version          = '0.1.1'
  s.summary          = 'Common UIKit extension, utilities and custom views, view controllers for iOS '

  s.description      = 'Common UIKit extension, utilities and custom views, view controllers for iOS '

  s.homepage         = 'https://zgit.csez.zohocorpin.com/desk-mobile/zd-portal-utilities'
  s.license          = { :type => 'MIT', :text => 'LICENSE' }
  s.authors          = { 'Imthath M' => 'imthathullah.m@zohocorp.com'}
  s.source           = { :git => 'https://zgit.csez.zohocorpin.com/desk-mobile/zd-portal-utilities', :tag => s.version }
  s.platform         = :ios, '9.0'

  s.source_files = 'DPViewExtensions/Sources/**/*.{swift,h,m}'
  s.resources = 'DPViewExtensions/Sources/**/*.{ttf,js,css,gif,xib,xcassets}'
  s.public_header_files = 'DPViewExtensions/Sources/**/*.{h}'
  s.swift_version = "5.0"
  
end
