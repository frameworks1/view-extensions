//
//  DPConstant.swift
//  DPViewExtensions
//
//  Created by Imthath M on 25/11/19.
//  Copyright © 2019 Imthath. All rights reserved.
//

import Foundation
import UIKit

var DPBundle: Bundle { return Bundle(for: DPViewController.self) }

public class DPIcon {
    static var fontName: String = "icomoon"
}

class DPNotification {

    public static var themeChanged: NSNotification.Name {
        return NSNotification.Name("themeChanged")
    }
}

public extension CGFloat {
    static var one: CGFloat { return 1 }
    static var five: CGFloat { return 5 }
    static var ten: CGFloat { return 10 }
    static var sixteen: CGFloat { return 16 }
    static var twentyTwo: CGFloat { return 22 }
    static var twentySix: CGFloat { return 26 }
    static var hundred: CGFloat { return 100 }

    static var averageTouchSize: CGFloat { return 44 }

    static var seperatorThickness: CGFloat { return 0.5 }
}

public extension CGRect {

    static func square(of side: CGFloat) -> CGRect {
        return CGRect(width: side, height: side, x: 0, y: 0)
    }
}

extension DPIcon {
    public static var verticalDots: String { "\u{e9a9}" }
    public static var horizontalDots: String { "\u{e9a2}" }
    public static var dropDown: String { "\u{e9bb}" }
    public static var comment : String { "\u{e9ad}" }
    public static var thumbsUp: String { "\u{e945}" }
    public static var search: String { "\u{e918}" }
    
    public static var smallSize: CGFloat { 16 }
    public static var mediumSize: CGFloat { 22 }
    
}

public struct DPFont {
    public static var extraLargeTitle: UIFont { return UIFont.preferredFont(forTextStyle: .title2) }
    public static var largeTitle: UIFont { return UIFont.preferredFont(forTextStyle: .title3) }
    public static var title: UIFont { return UIFont.preferredFont(forTextStyle: .headline) }
    public static var primary: UIFont { return UIFont.preferredFont(forTextStyle: .body) }
    public static var secondary: UIFont { return UIFont.preferredFont(forTextStyle: .callout) }
    public static var footnote: UIFont { return UIFont.preferredFont(forTextStyle: .footnote) }
}

