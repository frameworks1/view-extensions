//
//  OptionalExtension.swift
//  DPViewExtensions
//
//  Created by Imthath M on 12/12/19.
//  Copyright © 2019 Imthath. All rights reserved.
//

import Foundation

public protocol Defaultable {
    static var baseValue: Self { get }
}

extension String: Defaultable {
    public static var baseValue: String { return "" }
}

extension Bool: Defaultable {
    public static var baseValue: Bool { return false }
}

extension Int: Defaultable {
    public static var baseValue: Int { return 0 }
}

extension Float: Defaultable {
    public static var baseValue: Float { return 0 }
}

extension Double: Defaultable {
    public static var baseValue: Double { return 0 }
}

extension Optional where Wrapped: Defaultable {

    public var defaultUnwrap: Self {
        if let strongSelf = self {
            return strongSelf
        }

        return Wrapped.baseValue
    }
}

extension Optional {

    public func safeUnwrap(to object: inout Wrapped?) {
        switch self {
        case .none:
            return
        case .some(let value):
            object = value
        }
    }

    public func safeUnwrap(to object: inout Wrapped) {
        switch self {
        case .none:
            return
        case .some(let value):
            object = value
        }
    }

    public var isNil: Bool {
        switch self {
        case .none:
            return true
        case .some:
            return false
        }
    }
}

extension Optional where Wrapped == String {

    public var number: Int {

        if let value = self,
            let number = Int(value) {
                return number
        }

        return 0
    }
}

extension Optional {
    public var hasValue: Bool {
        switch self {
        case .none:
            return false
        case .some:
            return true
        }
    }
}
