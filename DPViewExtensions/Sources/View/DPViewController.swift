//
//  DPViewController.swift
//  DPViewExtensions
//
//  Created by Imthath M on 23/09/19.
//  Copyright © 2019 Imthath M. All rights reserved.
//

import UIKit
import WebKit

open class DPViewController: UIViewController {
    
    public lazy var messageView = UIView(frame: .square(of: .hundred))
    public lazy var messageLabel = DPLabel(color: .primaryText, font: DPFont.primary)
    
    public func layoutMessageView() {
        
        view.addSubview(messageView)
        alignMessageView()
        
        messageView.addSubview(messageLabel)
        messageLabel.alignHorizontally(with: messageView, offset: .sixteen)
        messageLabel.align(.centerY, with: messageView, offset: -(view.frame.height * 0.1))
        
        messageLabel.textAlignment = .center
        messageLabel.numberOfLines = 0
    }
    
    open func alignMessageView() {
        messageView.alignSafe(with: view)
    }
    
    public func showErrorView(withText text: String) {
        hideLoader()
        messageView.show()
        messageLabel.text = text
    }
    
}

extension DPViewController {
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        updateThemeOnLoading()
        extendedLayoutIncludesOpaqueBars = true
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarTheme(using: DPTheme.current.navBarTheme)
    }
    
    override open func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateTheme(basedOn: previousTraitCollection)
    }
    
}

open class DPTableViewController: UITableViewController, Themable {
    
    open func registerCells() {
        "override this function to register cells for the table view".log()
    }
    
    open func setColors() {
        tableView.backgroundColor = DPTheme.primaryBackGroundColor
        tableView.separatorColor = DPTheme.borderColor
        tableView.setColorsForVisibleCells()
    }
}

extension DPTableViewController {
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        updateThemeOnLoading()
        registerCells()
        extendedLayoutIncludesOpaqueBars = true
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarTheme(using: DPTheme.current.navBarTheme)
    }
    
    override open func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateTheme(basedOn: previousTraitCollection)
    }
}

fileprivate extension UIViewController {
    
    func updateThemeOnLoading() {
        if #available(iOS 13.0, *),
            DPTheme.style == .system {
            DPTheme.update(basedOn: traitCollection.userInterfaceStyle)
        }
        applyTheme()
    }
    
    func updateTheme(basedOn previousTraitCollection: UITraitCollection?) {
        if #available(iOS 13.0, *),
            DPTheme.style == .system,
            previousTraitCollection?.userInterfaceStyle != traitCollection.userInterfaceStyle {
            DPTheme.update(basedOn: traitCollection.userInterfaceStyle)
            navigationController?.setNavigationBarTheme(using: DPTheme.current.navBarTheme)
            NotificationCenter.default.post(Notification(name: DPNotification.themeChanged))
            applyTheme()
        }
    }
    
    func applyTheme() {
        view.backgroundColor = DPTheme.primaryBackGroundColor
        if let themedVC = self as? Themable {
            themedVC.setColors()
        } else {
            assertionFailure("Confirm \(self) to Themable")
        }
    }
}
