//
//  StringExtension.swift
//  DPViewExtensions
//
//  Created by Imthath M on 12/12/19.
//  Copyright © 2019 Imthath. All rights reserved.
//

import Foundation
import UIKit

public extension String {

    var prependedDot: String {
        return " • " + self
    }

    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }

    var encoded: String? {
        let characterSet = CharacterSet(charactersIn: "=+&:,'\"#%/<>?@\\^`{|} ")

        if let result = self.addingPercentEncoding(withAllowedCharacters: characterSet.inverted) {
            return result
        }

        return nil
    }

    var extn: String {
        if let extn = self.split(separator: ".").last {
            return String(extn)
        }

        return self
    }

    var isNumber: Bool {
        return Int(self) != nil
    }

    var byteSize: String {
        let number: Int64 = Int64(self) ?? 0
        return ByteCountFormatter.string(fromByteCount: number, countStyle: .memory)
    }

    var isValidEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}"
                                                + "\\@"
                                                + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}"
                                                + "("
                                                + "\\."
                                                + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}"
                                                + ")+", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSRange(location: 0, length: self.count)) != nil
        } catch {
            return false
        }
    }

    var number: Int {
        return Int(self) ?? 0
    }

    func regexMatch(with string: String) -> Bool {
        return !isEmpty && range(of: string, options: .regularExpression) == nil
    }

    func prependIcon(_ icon: String, size: CGFloat = .ten) -> NSAttributedString {
//        let fullText = icon + " " + self
//        return fullText.applyIconFont(to: icon, size: size)
        let givenText = NSAttributedString(string: " " + self, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: size)])
        guard let iconFont = UIFont(name: DPIcon.fontName, size: size) else {
            return givenText
        }

        let iconText = NSMutableAttributedString(string: icon, attributes: [NSAttributedString.Key.font: iconFont])
        iconText.append(givenText)
        return iconText
    }

    func appendIcon(_ icon: String, size: CGFloat = .ten) -> NSAttributedString {
        let fullText = self + " " + icon
        return fullText.applyIconFont(to: icon, size: size)
    }

    func applyIconFont(to icon: String, size: CGFloat = .ten) -> NSAttributedString {
        guard let iconFont = UIFont(name: DPIcon.fontName, size: size) else {
            return NSAttributedString(string: self)
        }

        return apply(font: iconFont, to: icon)
    }

    func apply(font: UIFont, to text: String) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttribute(NSAttributedString.Key.font, value: font, range: (self as NSString).range(of: text))
        return attributedString
    }

    func withColor(_ color: UIColor, forText text: String) -> NSAttributedString {
        let result =  NSMutableAttributedString(string: self)
        result.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSString(string: self.lowercased()).range(of: text.lowercased()))
        return result
    }

    func date(withFormat format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
}

internal extension String {

    func logAndAssertFailure() {
        self.log()
        assertionFailure(self)
    }

    func log(file: String = #file,
             functionName: String = #function,
             lineNumber: Int = #line) {
        print("\(URL(fileURLWithPath: file).lastPathComponent)-\(functionName):\(lineNumber)  \(self)")
    }

    func localized(for type: AnyClass, fromTable name: String) -> String {
        return NSLocalizedString(self, tableName: name, bundle: Bundle(for: type.self), value: "", comment: "")
    }

}
