//
//  UIKitExtensions.swift
//  DPViewExtensions
//
//  Created by Imthath M on 21/10/19.
//  Copyright © 2019 Imthath M. All rights reserved.
//

import UIKit


public extension UIColor {

    convenience init(hexCode: String) {

        let hex = hexCode.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32

        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            self.init(white: 0.0, alpha: 0.0)
            return
        }

        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }

    convenience init(red: Int, green: Int, blue: Int, alpha: CGFloat = 1) {
        self.init(red: CGFloat(red)/255, green: CGFloat(green)/255, blue: CGFloat(blue)/255, alpha: alpha)
    }

    convenience init(rbg: Int, alpha: CGFloat = 1) {
        self.init(red: CGFloat(rbg)/255, green: CGFloat(rbg)/255, blue: CGFloat(rbg)/255, alpha: alpha)
    }
    
    var hexCode: String? {

        guard let components = self.cgColor.components,
            components.count > 3 else { return nil }
        
        let r: Float =  Float(components[0])
        let g: Float = Float(components[1])
        let b: Float = Float(components[2])

        return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
    }
}

public extension UIView {

    @inlinable func hide() {
        self.isHidden = true
    }

    @inlinable func show() {
        self.isHidden = false
    }

    @inlinable func roundCorners(by radius: CGFloat) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }

    @inlinable func round() {
        if frame.size.height != frame.size.width {
            assertionFailure("can't make round when height is not equal to width")
        }

        layer.cornerRadius = frame.size.width * 0.5
    }

    @inlinable func removeAllSubViews() {
        self.subviews.forEach({ $0.removeFromSuperview() })
    }
}

public extension CGRect {

    init(width: Int = 0, height: Int = 0, x: Int = 0, y: Int = 0) {
        self.init(x: x, y: y, width: width, height: height)
    }

    init(width: CGFloat = 0, height: CGFloat = 0, x: CGFloat = 0, y: CGFloat = 0) {
        self.init(x: x, y: y, width: width, height: height)
    }
}

public extension UIEdgeInsets {

    static func allSides(by padding: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
    }
}

public extension IndexPath {

    func isLastCell(of tableView: UITableView) -> Bool {

        // check if it is not the last section
        if section + 1 != tableView.numberOfSections {
            return false
        }

        // check if it is not the last row
        if row + 1 != tableView.numberOfRows(inSection: section) {
            return false
        }

        // it is the last row in the last section
        return true
    }
}

public extension UITableView {

    /// generic method returns the inferred type of cell using identifier from Reusable protocol
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Unable to Dequeue Reusable Table View Cell with identitfier \(T.reuseIdentifier)")
        }

        return cell
    }

    func showLoaderInFooter() {
        let activity = UIActivityIndicatorView(frame: .square(of: .averageTouchSize * 2))
        tableFooterView = activity
        activity.startAnimating()
    }

    func hideLoaderInFooter() {
        tableFooterView = nil
    }
}

extension UITableViewCell {
    public static var reuseIdentifier: String { return String(describing: self) }
}

var handle = 8

extension UIBarButtonItem {

    public convenience init(image: UIImage?, action: Selector, target: Any) {
        let button: UIButton = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.addTarget(target, action: action, for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: .twentySix, height: .twentySix)
        self.init(customView: button)
    }

    private var badgeLayer: CAShapeLayer? {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as? CAShapeLayer
        } else {
            return nil
        }
    }

    public func setCount(_ count: Int, withColor color: UIColor) {
        setBadge(text: "\(count)", andColor: color)
    }

    public func setBadge(text: String?, withOffsetFromTopRight offset: CGPoint = CGPoint.zero, andColor color: UIColor = UIColor.red, andFilled filled: Bool = true, andFontSize fontSize: CGFloat = 11) {
        badgeLayer?.removeFromSuperlayer()

        if (text == nil || text == "" || text == "0") {
            return
        }

        addBadge(text: text!, withOffset: offset, andColor: color, andFilled: filled)
    }

    private func addBadge(text: String, withOffset offset: CGPoint = CGPoint.zero, andColor color: UIColor = UIColor.red, andFilled filled: Bool = true, andFontSize fontSize: CGFloat = 11) {

        guard let view = self.customView else { return }

        var font = UIFont.systemFont(ofSize: fontSize)
        font = UIFont.monospacedDigitSystemFont(ofSize: fontSize, weight: UIFont.Weight.regular)

        let badgeSize = text.size(withAttributes: [NSAttributedString.Key.font: font])

        // Initialize Badge
        let badge = CAShapeLayer()

        let height = badgeSize.height
        var width = badgeSize.width + 2 /* padding */

        //make sure we have at least a circle
        if (width < height) {
            width = height
        }

        //x position is offset from right-hand side
        let x = view.frame.width - width + offset.x

        let badgeFrame = CGRect(origin: CGPoint(x: x, y: offset.y), size: CGSize(width: width, height: height))

        badge.drawRoundedRect(rect: badgeFrame, andColor: color, filled: filled)
        view.layer.addSublayer(badge)

        // Initialiaze Badge's label
        let label = CATextLayer()
        label.string = text
        label.alignmentMode = CATextLayerAlignmentMode.center
        label.font = font
        label.fontSize = font.pointSize

        label.frame = badgeFrame
        label.foregroundColor = filled ? UIColor.white.cgColor : color.cgColor
        label.backgroundColor = UIColor.clear.cgColor
        label.contentsScale = UIScreen.main.scale
        badge.addSublayer(label)

        // Save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, badge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }

    public func removeBadge() {
        badgeLayer?.removeFromSuperlayer()
    }
}

extension CAShapeLayer {
    func drawRoundedRect(rect: CGRect, andColor color: UIColor, filled: Bool) {
        fillColor = filled ? color.cgColor : UIColor.white.cgColor
        strokeColor = color.cgColor
        path = UIBezierPath(roundedRect: rect, cornerRadius: 7).cgPath
    }
}

public extension UIButton {

    func alignContentLeading() {
        if #available(iOS 11.0, *) {
            contentHorizontalAlignment = .leading
        } else {
            contentHorizontalAlignment = .left
        }
    }
    
    func alignContentTrailing() {
        if #available(iOS 11.0, *) {
            contentHorizontalAlignment = .trailing
        } else {
            contentHorizontalAlignment = .right
        }
    }
    
    func enable() {
        isEnabled = true
    }

    func disable() {
        isEnabled = false
    }
}

var searchIcon: UIView?

extension UISearchBar {

    var textField: UITextField? {
        if #available(iOS 13.0, *) {
            return searchTextField
        }

        let subViews = subviews.flatMap { $0.subviews }
        guard let textField = (subViews.filter { $0 is UITextField }).first as? UITextField else {
            return nil
        }
        return textField
    }

    var isActivityIndicator: Bool {
        return textField?.leftView is UIActivityIndicatorView
    }

    public func startLoading() {
        if !isActivityIndicator {
            let newActivityIndicator = UIActivityIndicatorView()
            newActivityIndicator.startAnimating()
            searchIcon = textField?.leftView
            textField?.leftView = newActivityIndicator
        }
    }

    public func stopLoading() {
        textField?.leftView = searchIcon
    }

    public var isLoading: Bool {
        get {
            return isActivityIndicator
        } set {
            if newValue {
                startLoading()
            } else {
                stopLoading()
            }
        }
    }
}

public extension UIAlertController {

    static func initialize(title: String?, message: String?, preferredStyle: UIAlertController.Style) -> UIAlertController{
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        NotificationCenter.default.addObserver(alert, selector: #selector(setColors), name: DPNotification.themeChanged, object: nil)
        alert.setColors()
        return alert
    }
    
    @objc func setColors() {
        // TODO: implement colors for all elements
        // view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = DPTheme.secondaryBackGroundColor
        view.tintColor = DPTheme.interactiveColor
    }
}
