//
//  DPPicker.swift
//  DPViewExtensions
//
//  Created by Imthath M on 09/12/19.
//  Copyright © 2019 Imthath. All rights reserved.
//

import UIKit

public protocol DPPickerDelegate: class {
    func selected<AnyChoosable: Choosable>(_ choice: AnyChoosable, withTag tag: Int)
}

public class DPPicker<Element: Choosable & Equatable>: UIButton, Themable, UIPickerViewDataSource, UIPickerViewDelegate {

    public override var inputView: UIView? { picker }

    public override var inputAccessoryView: UIView? { toolbar }

    public override var canBecomeFirstResponder: Bool { isEnabled }

    var selectedItem: Element? {
        didSet { updateTitle() }
    }

    public override var isEnabled: Bool {
        didSet { updateTitle() }
    }

    var choices: [Element]
    let picker = UIPickerView()
    let toolbar = UIToolbar()
    weak var delegate: DPPickerDelegate?

    public init(choices: [Element], selected: Element? = nil,
                delegate: DPPickerDelegate, isChoosable: Bool = true,
                frame: CGRect = .square(of: .hundred), tag: Int = 1) {
        self.choices = choices
        super.init(frame: frame)
        self.isEnabled = isChoosable
        self.tag = tag
        self.delegate = delegate
        configureViews()
        updateSelection(with: selected)
    }

    public func updateSelection(with choice: Element?) {
        if let selected = choice,
            let selectedIndex = choices.firstIndex(of: selected) {
            selectedItem = selected
            picker.selectRow(selectedIndex, inComponent: 0, animated: true)
        } else {
            selectedItem = choices.first
        }
    }

    fileprivate func updateTitle() {
        if isEnabled, choices.count > 1 {
            setAttributedTitle(selectedItem?.displayString.appendIcon(DPIcon.dropDown), for: .normal)
        } else {
            if let string = selectedItem?.displayString {
                setAttributedTitle(NSAttributedString(string: string), for: .normal)
            } else {
                setAttributedTitle(nil, for: .normal)
            }
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func update(_ choices: [Element]) {
        self.choices = choices
        selectedItem = choices.first
    }

    @objc public func showPicker() {
        if choices.count > 1 {
            becomeFirstResponder()
        }
    }

    @objc public func dismiss() {
        resignFirstResponder()
    }

    func configureViews() {
        addTarget(self, action: #selector(showPicker), for: .touchDown)
        toolbar.sizeToFit()
        toolbar.isTranslucent = false
        toolbar.barStyle = .default

        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismiss))
        toolbar.setItems([UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                          doneButton], animated: true)

        picker.delegate = self
        picker.dataSource = self
    }

    public func setColors() {
        backgroundColor = DPTheme.primaryBackGroundColor
        setTitleColor()
        
        picker.tintColor = DPTheme.primaryTextColor
        picker.backgroundColor = DPTheme.secondaryBackGroundColor

        toolbar.barTintColor = DPTheme.secondaryBackGroundColor
        toolbar.tintColor = DPTheme.interactiveColor
        
    }

    func setTitleColor() {
//        setTitleColor(DPTheme.primaryTextColor, for: .normal)
        titleLabel?.textColor = DPTheme.primaryTextColor
    }

    // MARK: - Picker Data Source
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return choices.count
    }

    // MARK: - Picker Delegate
//    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return choices[row].displayString
//    }
    
    public func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return choices[row].displayString.attributed(with: DPTheme.primaryTextColor)
    }

    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedItem = choices[row]
        delegate?.selected(choices[row], withTag: self.tag)
    }
}

public class DPIconPicker<Element: Choosable & Iconable & Equatable>: DPPicker<Element> {
    override func configureViews() {
        super.configureViews()
        titleLabel?.font = UIFont(name: DPIcon.fontName, size: .twentyTwo)
    }

    override func updateTitle() {
        setTitle(selectedItem?.iconString, for: .normal)
        setTitleColor()
    }

    override func setTitleColor() {
        setTitleColor(selectedItem?.color, for: .normal)
    }
    
    override public func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return choices[row].selectionText.attributed(with: DPTheme.primaryTextColor)
    }
}

extension String {
    func attributed(with color: UIColor) -> NSAttributedString {
        return NSAttributedString(string: self, attributes: [.foregroundColor: color])
    }
}
