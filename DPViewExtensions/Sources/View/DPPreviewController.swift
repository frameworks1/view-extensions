//
//  DPPreviewController.swift
//  DPViewExtensions
//
//  Created by Imthath M on 24/10/19.
//  Copyright © 2019 Imthath M. All rights reserved.
//

import QuickLook

public protocol DPPreviewDelegate: class {
    func downloadImage(fromURL url: String, onCompletion handler: (Result<Data, Error>) -> Void)
}

open class DPPreviewController: QLPreviewController {

    var items: [DPPreviewItem]
    weak var downloader: DPPreviewDelegate?
//    var currentIndex: Int

    public init(attachments: [DPAttachment], index: Int) {
        self.items = attachments.compactMap { DPPreviewItem(attachment: $0) }
        super.init(nibName: nil, bundle: nil)
        dataSource = self
        currentPreviewItemIndex = index
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    open func download(_ item: DPPreviewItem, at index: Int) {
        downloader?.downloadImage(fromURL: item.attachment.url) { result in
            self.handle(result, for: item, at: index)
        }
    }
}

extension DPPreviewController: QLPreviewControllerDataSource {

    public func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return items.count
    }

    public func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        if items[index].previewItemURL.isNil {
            download(items[index], at: index)
//            view.makeToastActivity(.center)
//        } else {
//            view.hideToastActivity()
            return Loader.shared
        }

        return items[index]
    }
    
    public func handle(_ result: Result<Data, Error>, for item: DPPreviewItem, at index: Int) {
        switch result {
        case .success(let data):
            item.previewItemURL = DPFileManager.save(data, withName: item.attachment.name, inFolder: IMAGES_FOLDER + "/" + item.attachment.id)
            item.previewItemTitle = item.attachment.name
            if self.currentPreviewItemIndex == index {
                self.refreshCurrentPreviewItem()
            } else {
                self.reloadData()
            }
        case .failure(let error):
            "\(error)".log()
        }
    }
}

public class DPPreviewItem: NSObject, QLPreviewItem {
    public var attachment: DPAttachment
    var isDownloading = false

    init(attachment: DPAttachment) {
        self.attachment = attachment
    }

    public var previewItemURL: URL?
    public var previewItemTitle: String?

}

class Loader: NSObject, QLPreviewItem {

    private override init() { }

    public static let shared = Loader()

    var previewItemURL: URL? { return DPBundle.url(forResource: "loading", withExtension: "gif") }

}

enum DPDownloadState {
    case idle, running, failure, success
}

public extension UIAlertController {
    static func perfromExitCheck(on viewController: UIViewController) {
        let alert = UIAlertController(title: "Are you sure want to cancel?", message: "Your changes will not be saved", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive, handler: { _ in viewController.dismiss(animated: true) } )
        let no = UIAlertAction(title: "No", style: .default)
        alert.addAction(yes)
        alert.addAction(no)
        viewController.present(alert, animated: true)
    }
}

public extension Cancellable {

    func addCancelButton() {
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(defaultCancelAction))

        if var leftItems = navigationItem.leftBarButtonItems {
            leftItems.append(cancel)
        } else {
            navigationItem.leftBarButtonItem = cancel
        }

    }
}

public extension UIViewController {

    @objc func defaultCancelAction() {
        if let cancellableVC = self as? Cancellable,
           cancellableVC.stateChanged {
            UIAlertController.perfromExitCheck(on: self)
        } else {
            self.dismiss(animated: true)
        }
    }
}
