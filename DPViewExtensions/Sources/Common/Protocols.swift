//
//  Protocols.swift
//  DPViewExtensions
//
//  Created by Imthath M on 12/12/19.
//  Copyright © 2019 Imthath. All rights reserved.
//

import Foundation
import UIKit

/// Provided conformance to UITableViewCell.
/// Hence all subclasses can directly use the static variable.
public protocol Reusable {
    static var reuseIdentifier: String { get }
}

/// Conform required subclasses of UITableViewCell
public protocol Registerable: UITableViewCell {
    static func register(to tableView: UITableView)
}

/// Conform required view controllers and cells to change theme at runtime.
public protocol Themable {
    func setColors()
}

/// Type used in DPPicker to display list of options
public protocol Choosable {
    var displayString: String { get }
}

/// Used in DPPicker where is selection is done using Icons
public protocol Iconable {
    var selectionText: String { get }
    var iconString: String { get }
    var color: UIColor { get }
}

/// UITableView conforms to the protocol and the updates are performed inside performBatchUpdates based on the OS version
public protocol Updatable: class {
    func perform(_ updates: @autoclosure () -> Void)
    func performWithoutAnimation(_ updates: @autoclosure () -> Void)
}

/// To check and dismiss view controllers, with a Cancel button on left side of Navigation Bar
public protocol Cancellable: UIViewController {
    var stateChanged: Bool { get }
    func addCancelButton()
}

public protocol Imitable: Codable {
    var copy: Self? { get }
}

extension Imitable {
    public var copy: Self? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return try? JSONDecoder().decode(Self.self, from: data)
    }
}

public extension Registerable {
     static func register(to tableView: UITableView) {
        tableView.register(Self.self, forCellReuseIdentifier: Self.reuseIdentifier)
    }
}

extension UITableView: Updatable {

    public func perform(_ updates: @autoclosure () -> Void) {
        if #available(iOS 11.0, *) {
            self.performBatchUpdates(updates, completion: nil)
        } else {
            self.beginUpdates()
            updates()
            self.endUpdates()
        }
    }
    
    public func performWithoutAnimation(_ updates: @autoclosure () -> Void) {
        UIView.performWithoutAnimation {
            perform(updates())
        }
    }
}
