//
//  ContentView.swift
//  DPViewExtensions
//
//  Created by Imthath M on 28/02/20.
//  Copyright © 2020 Imthath. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
