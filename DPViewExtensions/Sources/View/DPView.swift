//
//  DPView.swift
//   DPViewExtensions
//
//  Created by Imthath M on 01/10/19.
//  Copyright © 2019 Imthath M. All rights reserved.
//

import UIKit


var imagesDict = [String: UIImage]()

open class DPSectionHeaderView: UITableViewHeaderFooterView, Themable {
    var label: DPLabel
    var contentBackground: DPColor?
    
    public init(header: String?, textColor: DPColor, font: UIFont,
                contentBackground: DPColor? = .primaryBG) {
        self.label = DPLabel(color: textColor, title: header, font: font)
        self.contentBackground = contentBackground
//        self.label.text = header
        super.init(reuseIdentifier: "testing")
//        textLabel?.text = nil
        layoutViews()
        NotificationCenter.default.addObserver(self, selector: #selector(setColors), name: DPNotification.themeChanged, object: nil)
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc public func setColors() {
        contentView.backgroundColor = contentBackground?.value
        backgroundView = UIView(frame: self.frame)
        backgroundView?.backgroundColor = .clear
        label.setColors()
    }

    func layoutViews() {
        contentView.addSubview(label)
        label.alignHorizontally(with: contentView, offset: .sixteen)
        label.align(.top, with: contentView, offset: .sixteen)
        label.align(.bottom, with: contentView, offset: -.ten)

        setColors()
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: DPNotification.themeChanged, object: nil)
    }
}

open class DPSeperatedSectionHeader: DPSectionHeaderView {

    private lazy var bottomSeparator = UILabel()

    override func layoutViews() {
        super.layoutViews()
        contentView.addSubview(bottomSeparator)
        bottomSeparator.alignBottom(with: contentView)
        bottomSeparator.fixHeight(.seperatorThickness)
    }

    override public func setColors() {
        super.setColors()
        bottomSeparator.backgroundColor = DPTheme.borderColor
    }
}

open class DPIconButton: UIButton {

    public init(icon: String, size: CGFloat = .twentyTwo) {
        super.init(frame: .square(of: .averageTouchSize))
        titleLabel?.font = UIFont(name: DPIcon.fontName, size: size)
        setTitle(icon, for: .normal)
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

open class DPRoundedIconButton: DPIconButton, Themable {

    let textColor: DPColor
    let bgColor: DPColor
    public init(icon: String, color: DPColor, backgroundColor: DPColor, size: CGFloat = .twentyTwo) {
        self.textColor = color
        self.bgColor = backgroundColor
        super.init(icon: icon, size: size)
        roundCorners(by: size)
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func setColors() {
        backgroundColor = bgColor.value
        setTitleColor(textColor.value, for: .normal)
    }

}

open class DPIconLabel: DPLabel {

   public init(color: DPColor, title: String? = nil, size: CGFloat = DPIcon.mediumSize) {

    super.init(color: color, title: title, font: UIFont(name: DPIcon.fontName, size: size))
        text = title
        textAlignment = .center
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

open class DPLabel: UILabel, Themable {

    var color: DPColor

    public init(color: DPColor, title: String? = nil,
         frame: CGRect = .zero, font: UIFont? = nil) {
        self.color = color
        super.init(frame: frame)
        self.font = font
        text = title
        setColors()
    }

    public func setColors() {
        self.textColor = color.value
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

open class DPSpacedLabel: DPLabel {
    var horizontalPadding: CGFloat
    var verticalPadding: CGFloat
    
    public init(color: DPColor, title: String? = nil,
                frame: CGRect = .zero, font: UIFont? = nil,
                horizontal: CGFloat = .ten, vertical: CGFloat = .five) {
        verticalPadding = vertical
        horizontalPadding = horizontal
        super.init(color: color, title: title, frame: frame, font: font)
        textAlignment = .center
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func increaseContentSize(alongX horizontal: CGFloat, alongY vertical: CGFloat) {
        verticalPadding = vertical
        horizontalPadding = horizontal
    }
    
    override public var intrinsicContentSize: CGSize {
            var size = super.intrinsicContentSize
            size.width += horizontalPadding
            size.height += verticalPadding
            return size
    }
}

open class DPInteractiveCell: UITableViewCell, Registerable, Themable {

    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectedBackgroundView = UIView()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setColors() {
        selectedBackgroundView?.backgroundColor = DPTheme.placeholderBackGroundColor
    }
}

open class DPIconCell: DPInteractiveCell {

    public typealias ImageDownloader = (String, ((Result<Data, Error>) -> Void)) -> Void
    
    public var icon: UIImageView = UIImageView(frame: .square(of: .averageTouchSize * 1.5))

    open func setIcon(fromURL url: String?, forName name: String,
                      using usecase: ImageDownloader) {
        if let urlString = url,
            let image = imagesDict[urlString] {
            icon.image = image
        } else {
            icon.setImage(string: name, color: .gray, circular: true,
                          textAttributes: [NSAttributedString.Key.foregroundColor: UIColor.white,
                                           NSAttributedString.Key.font: UIFont.systemFont(ofSize: .twentyTwo)])
            updateImage(from: url, using: usecase)
        }
        icon.roundCorners(by: .five)
    }

    open func updateImage(from url: String?, using usecase: ImageDownloader) {

        guard let urlString = url else { return }

        usecase(urlString) { [weak self] result in

            guard let strongSelf = self else { return }

            switch result {
            case .success(let data):
                if let image = UIImage(data: data) {
                    strongSelf.icon.image = image
                    imagesDict[urlString] = image
                } else {
                    "Image data parse failure - \(data)".log()
                }
            case .failure(let error):
                "Image fetch failure - \(error)".log()
            }
        }
    }
}

open class DPNonInteractiveCell: UITableViewCell, Registerable {

    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none

    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

open class DPTableView: UITableView, Themable {

    override public init(frame: CGRect = .square(of: .hundred), style: UITableView.Style = .plain) {
        super.init(frame: frame, style: style)
        setColors()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func setColors() {
        backgroundColor = .clear
        separatorColor = DPTheme.borderColor
        setColorsForVisibleCells()
    }
}

public extension UITableView {

    func setColorsForVisibleCells() {
        guard let indices = indexPathsForVisibleRows else { return }

        for indexPath in indices {
            if let cell = cellForRow(at: indexPath) {
                if let themedCell = cell as? Themable {
                    themedCell.setColors()
                } else {
                    "\(String(describing: cell)) not confirmed to Themable protocol".logAndAssertFailure()
                }
            }
        }
    }
}

public extension UICollectionView {

    func setColorsForVisibleCells() {

        for indexPath in indexPathsForVisibleItems {
            if let cell = cellForItem(at: indexPath) as? Themable {
                cell.setColors()
            }
        }
    }
}

open class DPTextField: UITextField {

    var paddingLeft: CGFloat

    public init(placeholder: String? = nil, leftPadding: CGFloat = .zero) {
        paddingLeft = leftPadding
        super.init(frame: .zero)
        self.placeholder = placeholder

        setLeftPadding()
        setColors()
    }

    private func setLeftPadding() {
        guard !paddingLeft.isZero else { return }

        let leftPaddingView = UIView(frame: CGRect(width: paddingLeft))
        leftView = leftPaddingView
        leftViewMode = .always
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension DPTextField: Themable {

    public func setColors() {
        backgroundColor = DPTheme.placeholderBackGroundColor
        textColor = DPTheme.primaryTextColor
    }
}

open class DPTextView: UITextView {

    let placeholder: String

    public init(placeholder: String, font: UIFont = DPFont.secondary,
                useCustomColors: Bool = false, useCustomDelegate: Bool = false ) {
        self.placeholder = placeholder
        super.init(frame: .zero, textContainer: nil)
        text = placeholder
        self.font = font
        textContainerInset = UIEdgeInsets.allSides(by: .ten)

        if !useCustomColors {
            setColors()
        }

        if !useCustomDelegate {
            delegate = self
        }
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public var enteredText: String? {
        guard text != placeholder,
            !text.isEmpty else {
                return nil
        }

        return text
    }
}

extension DPTextView: Themable {

    public func setColors() {
        backgroundColor = DPTheme.placeholderBackGroundColor
        if text == placeholder {
            textColor = DPTheme.placeholderTextColor
        } else {
            textColor = DPTheme.primaryTextColor
        }
    }
}

extension DPTextView: UITextViewDelegate {
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if textColor == DPTheme.placeholderTextColor {
            text = nil
            textColor = DPTheme.primaryTextColor
        }
    }

    public func textViewDidEndEditing(_ textView: UITextView) {
        if text.isEmpty {
            text = placeholder
            textColor = DPTheme.placeholderTextColor
        }
    }
}

extension UIFont {

    @inlinable func getScaledFont(forStyle fontStyle: UIFont.TextStyle) -> UIFont {
        return self.withSize(UIFontDescriptor.preferredFontDescriptor(withTextStyle: fontStyle).pointSize)
    }
}
