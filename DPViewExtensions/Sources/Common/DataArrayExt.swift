//
//  DataArrayExt.swift
//  DPViewExtensions
//
//  Created by Imthath M on 12/12/19.
//  Copyright © 2019 Imthath. All rights reserved.
//

import Foundation

public extension Array where Element: Equatable {

    func firstMatch(of element: Element) -> Element? {
        if let index = firstIndex(of: element) {
            return self[index]
        }

        return nil
    }

    func isSameAs(_ other: [Element]) -> Bool {
        guard self.count == other.count else {
            return false
        }

        for (index, element) in self.enumerated() where element != other[index] {
            return false
        }

        return true
    }

    mutating func remove(_ item: Element ) {
        if let index = firstIndex(of: item) {
            remove(at: index)
        }
    }
}

public extension Array where Element: Hashable {

    var hasDuplicates: Bool {

        var dict = [Element: Bool]()

        for element in self {
            if let value = dict[element] {
                return value
            }

            dict[element] = true
        }

        return false
    }
}

extension Array where Element == IndexPath {

    public init(section: Int, with range: ClosedRange<Int>) {
        self.init()
        var index = range.lowerBound
        while index <= range.upperBound {
            self.append(IndexPath(row: index, section: section))
            index += 1
        }
    }
}

extension Array {

    public func element(at index: Int) -> Element? {

        guard index < self.count, index >= 0 else {
            return nil
        }

        return self[index]

    }

    public func seperate(by condition: (Element) -> Bool) -> ([Element], [Element]) {
        var matching = [Element]()
        var nonMatching = [Element]()
        
        for element in self {
            if condition(element) {
                matching.append(element)
            } else {
                nonMatching.append(element)
            }
        }
        
        return (matching, nonMatching)
    }
}

public extension Encodable {

    var jsonData: Data? {
        return try? JSONEncoder().encode(self)
    }

}

extension Data {

    static func fromURL(_ optionalURL: URL?) -> Data? {

        if let url = optionalURL {
            return try? Data(contentsOf: url)
        }

        return nil
    }

    internal var sizeInMB: Float {
        return Float(self.count) / Float(1024*1024)
    }

    internal var dictionary: [String: Any]? {
        do {
            return try JSONSerialization.jsonObject(with: self, options: .mutableLeaves) as? [String: Any]
        } catch {
            return nil
        }
    }

    internal func multiPartFormData(withName name: String, forFileType file: String) -> Data {
        let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
        let boundaryStart = "--\(boundary)\r\n"
        let boundaryEnd = "--\(boundary)--\r\n"
        let contentDispositionString = "Content-Disposition: form-data; name=\"\("file")\"; filename=\"\(name)\"\r\n"
        let contentTypeString = "Content-Type: \("image/jpeg")\r\n\r\n"

        let body  = NSMutableData()
        body.appendOptional(boundaryStart.data(using: String.Encoding.utf8))
        body.appendOptional(contentDispositionString.data(using: String.Encoding.utf8))
        body.appendOptional(contentTypeString.data(using: String.Encoding.utf8))
        body.append(self)
        body.appendOptional("\r\n".data(using: String.Encoding.utf8))
        body.appendOptional(boundaryEnd.data(using: String.Encoding.utf8))
        return body as Data
    }

}

extension NSMutableData {

    internal func appendOptional(_ other: Data?) {
        if let data = other {
            self.append(data)
        }
    }
}
