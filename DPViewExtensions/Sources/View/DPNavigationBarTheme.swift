//
//  DPNavigationBarTheme.swift
//  DPViewExtensions
//
//  Created by Imthath M on 03/10/19.
//  Copyright © 2019 Imthath M. All rights reserved.
//

import Foundation
import UIKit

@objc public protocol DPNavigationBarThemeProtocol {
    var titleColor: UIColor { get }
    var backgroundColor: UIColor? { get }
    var buttonColor: UIColor { get }
    var badgeColor: UIColor { get }
    var statusBarStyle: UIStatusBarStyle { get }
    var searchFieldColor: UIColor? { get }
    var isTranslucent: Bool { get }
}

@objc public class DPNavigationBarLightTheme: NSObject, DPNavigationBarThemeProtocol {

    private override init() { }

    internal static let shared = DPNavigationBarLightTheme()

    public var titleColor: UIColor { return .white }

    public var backgroundColor: UIColor? { UIColor(red: 0.22, green: 0.44, blue: 0.84, alpha: 1.00) }

    public var buttonColor: UIColor { return .white }

    public var badgeColor: UIColor { return DPTheme.failureColor }

    public var statusBarStyle: UIStatusBarStyle { return .default }

    public var searchFieldColor: UIColor? { DPTheme.primaryBackGroundColor }
    
    public var isTranslucent: Bool { return false }
}

@objc public class DPNavigationBarDarkTheme: NSObject, DPNavigationBarThemeProtocol {

    private override init() { }

    internal static let shared = DPNavigationBarDarkTheme()

    public var titleColor: UIColor { return DPTheme.primaryTextColor }

    public var backgroundColor: UIColor? { return DPTheme.secondaryBackGroundColor }

    public var buttonColor: UIColor { return DPTheme.primaryTextColor }

    public var badgeColor: UIColor { return DPTheme.failureColor }

    public var statusBarStyle: UIStatusBarStyle { return .lightContent }
    
    public var searchFieldColor: UIColor? { DPTheme.placeholderBackGroundColor }

    public var isTranslucent: Bool { return false }
}

extension UINavigationController {

    public func setNavigationBarTheme(using theme: DPNavigationBarThemeProtocol, and fontName: String = "") {
        view.backgroundColor =  DPTheme.primaryBackGroundColor
        
        if #available(iOS 13.0, *) {
            setNavigationBarAppearance(basedOn: theme)
        }
        
        navigationBar.barTintColor = theme.backgroundColor
        navigationBar.titleTextAttributes = [.foregroundColor: theme.titleColor]
        navigationBar.tintColor = theme.buttonColor
        navigationBar.isTranslucent = theme.isTranslucent
        
        if #available(iOS 11.0, *) {
            navigationBar.largeTitleTextAttributes = [.foregroundColor: theme.titleColor]
        }
    }
    
    @available(iOS 13.0, *)
    func setNavigationBarAppearance(basedOn theme: DPNavigationBarThemeProtocol) {
        let buttonAppearance = UIBarButtonItemAppearance()
        buttonAppearance.configureWithDefault(for: .plain)
        buttonAppearance.normal.titleTextAttributes = [.foregroundColor: theme.buttonColor]

        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.backgroundColor = theme.backgroundColor
        navBarAppearance.titleTextAttributes = [.foregroundColor: theme.titleColor]
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: theme.titleColor]

        
        navBarAppearance.backButtonAppearance = buttonAppearance
        navBarAppearance.buttonAppearance = buttonAppearance
        navBarAppearance.doneButtonAppearance = buttonAppearance

        navigationBar.scrollEdgeAppearance = navBarAppearance
        navigationBar.compactAppearance = navBarAppearance
        navigationBar.standardAppearance = navBarAppearance
    }
}

