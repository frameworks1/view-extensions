//
//  DPString.swift
//  DPViewExtensions
//
//  Created by Imthath M on 17/10/19.
//  Copyright © 2019 Imthath M. All rights reserved.
//

import Foundation

public protocol DPStringProtocol {
    var general: DPGeneralString { get }
    var date: DPDateString { get }
}

public protocol DPGeneralString {

    var done: String { get }
    var yes: String { get }
    var no: String { get }
    var alert: String { get }
    var close: String { get }
    var cancel: String { get }
    var dismiss: String { get }
    var delete: String { get }
    var save: String { get }
    var submit: String { get }
    var update: String { get }

    var backPressAlert: String { get }
    var deleteAlertTitle: String { get }
    var deleteAlertMessage: String { get }
}


public protocol DPDateString {
    var justNow: String { get }
    var minuteAgo: String { get }
    var minutesAgo: String { get }
    var hourAgo: String { get }
    var hoursAgo: String { get }
    var yesterday: String { get }
    var daysAgo: String { get }
    var weekAgo: String { get }
    var weeksAgo: String { get }
    var monthAgo: String { get }
    var monthsAgo: String { get }
    var yearAgo: String { get }
    var yearsAgo: String { get }
}

public class DPString {

    public static var general: DPGeneralString = DPGeneralEnglish()

    public static var date: DPDateString = DPDateEnglish()

    public static func updateStrings(with string: DPStringProtocol) {
        general = string.general
        date = string.date
    }
}

class DPGeneralEnglish: DPGeneralString {

    var done: String { "Done" }

    var yes: String { "Yes" }

    var no: String { "No" }

    var alert: String { "Alert" }

    var close: String { "Close" }

    var dismiss: String { "Dismiss" }

    var delete: String { "Delete" }

    var cancel: String { "Cancel" }
    
    var save: String { "Save" }
    
    var submit: String { "Submit" }
    
    var update: String { "Update" }

    var backPressAlert: String { "Are you sure you want to proceed? All attachments will be deleted." }

    var deleteAlertTitle: String { "Are you sure want to delete?" }

    var deleteAlertMessage: String { "This operation can not be reversed" }
}

class DPDateEnglish: DPDateString {

    var justNow: String { "just now"}

    var minuteAgo: String { "a minute ago"}

    var minutesAgo: String { "%@ minutes ago" }

    var hourAgo: String { "an hour ago" }

    var hoursAgo: String { "%@ hours ago" }

    var yesterday: String { "yesterday" }

    var daysAgo: String { "%@ days ago" }

    var weekAgo: String { "a week ago" }

    var weeksAgo: String { "%@ weeks ago" }

    var monthAgo: String { "a month ago" }

    var monthsAgo: String { "%@ months ago" }

    var yearAgo: String { "a year ago" }

    var yearsAgo: String { "%@ years ago" }

}

