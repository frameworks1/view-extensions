//
//  DPSearch.swift
//  DPViewExtensions
//
//  Created by Imthath M on 13/01/20.
//  Copyright © 2020 Imthath. All rights reserved.
//

import UIKit


public class DPSearchResultCell: DPInteractiveCell {
    
//    public var searchResult: String?
    
    let label = DPLabel(color: .primaryText, font: DPFont.secondary)
    let icon = DPIconLabel(color: .secondaryText, title: DPIcon.search)
    
    public func configureViews(_ searchResult: String) {
        label.text = searchResult
        
        layoutViews()
        setColors()
    }
    
    func layoutViews() {
        contentView.addSubview(icon)
        icon.align(.leading, with: contentView, offset: .sixteen)
        icon.align(.centerY, with: contentView)
        icon.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        
        contentView.addSubview(label)
        label.align(.trailing, with: contentView, offset: .five)
        label.align(.centerY, with: icon)
        label.align(.leading, with: icon, on: .trailing, offset: .sixteen)
    }
    
    override public func setColors() {
        super.setColors()
        label.setColors()
        icon.setColors()
    }
}

public typealias DPResultsController = UIViewController & UISearchResultsUpdating

public protocol DPSearchableController: UIViewController {
    var resultsController: DPResultsController { get }
    var searchController: UISearchController { get }
    var searchBoxPlaceholder: String { get }

    var tableView: UITableView? { get }
    func configureSearchController()
    func setColorsToSearchField()
}

public extension DPSearchableController {
    var tableView: UITableView? { nil }

    func configureSearchController() {
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = true
        } else {
            #warning("has white background error")
            tableView?.tableHeaderView = searchController.searchBar
            searchController.searchBar.barTintColor = .clear
        }

        if #available(iOS 9.1, *) {
            searchController.obscuresBackgroundDuringPresentation = false
        } else {
            searchController.dimsBackgroundDuringPresentation = false
        }

        if let searchBarDelegate = resultsController as? UISearchBarDelegate {
            searchController.searchBar.delegate = searchBarDelegate
        }
        
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.searchResultsUpdater = resultsController
        searchController.searchBar.placeholder = searchBoxPlaceholder
        setColorsToSearchField()
    }
    
    func setColorsToSearchField() {
        searchField?.backgroundColor = .red// DPTheme.placeholderBackGroundColor
        searchField?.tintColor = .blue // DPTheme.secondaryTextColor
        searchField?.textColor = .orange //DPTheme.primaryTextColor
        searchField?.leftView?.tintColor = .green //DPTheme.secondaryTextColor
    }
    
    var searchField: UITextField? {
        if #available(iOS 13.0 , *) {
            return searchController.searchBar.searchTextField
        }
        
        return searchController.searchBar.textField
    }
}

public protocol DPSearchable: class {
    var titleToSearch: String { get }
    var searchKeyword: String? { get set }

    func setKeyword(_ keyword: String)
    func highlightedKeyword(with color: UIColor) -> NSAttributedString
}

public extension DPSearchable {
    func setKeyword(_ keyword: String) {
        self.searchKeyword = keyword
    }

    func highlightedKeyword(with color: UIColor) -> NSAttributedString {
        if let keyword = searchKeyword {
            return titleToSearch.withColor(color, forText: keyword.trimmingCharacters(in: .whitespaces))
        }

        return NSAttributedString(string: titleToSearch)
    }
}
