//
//  DPTableViewController.swift
//  DPViewExtensions
//
//  Created by Imthath M on 31/10/19.
//  Copyright © 2019 Imthath M. All rights reserved.
//

import UIKit

open class DPListViewController: DPTableViewController {

    public lazy var messageView = UIView(frame: .square(of: .hundred))
    public lazy var messageLabel = DPLabel(color: .primaryText, font: DPFont.primary)

    public var sections: [SectionItem] = [] {
        didSet {
            if !isEmptyList {
                // NOTE: loader can sometimes be shown with the list
//                hideLoader()
                messageView.hide()
            }
        }
    }

    public func layoutMessageView() {

        view.addSubview(messageView)
        alignMessageView()

        messageView.addSubview(messageLabel)
        messageLabel.alignHorizontally(with: messageView, offset: .sixteen)
        messageLabel.align(.centerY, with: messageView, offset: -(view.frame.height * 0.1))
        
        messageLabel.textAlignment = .center
        messageLabel.numberOfLines = 0
    }
    
    open func alignMessageView() {
        messageView.alignSafe(with: view)
    }
    
    public func showErrorView(withText text: String) {
        guard isEmptyList else { return }
        
        hideLoader()
        messageView.show()
        messageLabel.text = text
    }

    public var isEmptyList: Bool {
        for section in sections where !section.items.isEmpty {
            return false
        }

        return true
    }
}

// MARK: - TableView Requirements
extension DPListViewController {

    override open func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let currentSection = sections.element(at: section) {
            return  currentSection.items.count
        }
        
        return .zero
    }

    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return sections[indexPath.section].items[indexPath.row].getCell(at: indexPath, of: tableView)
    }
}

extension DPListViewController {

    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[indexPath.section].items[indexPath.row].height
    }

    override open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return sections[section].header == nil ? .zero : UITableView.automaticDimension
    }

    override open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let title = sections[section].header {
            return DPSectionHeaderView(header: title, textColor: .secondaryText, font: DPFont.secondary)
        }

        return nil
    }

    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        sections[indexPath.section].items[indexPath.row].onSelect()
    }
}

public protocol ListItemProtocol {

    var height: CGFloat { get }

    func getCell(at indexPath: IndexPath, of tableView: UITableView) -> UITableViewCell

    func onSelect()
}

public extension ListItemProtocol {

    var height: CGFloat { return UITableView.automaticDimension }

    func onSelect() { }
}

// MARK: - Section Item
public class SectionItem {
    public let id: String
    public var header: String?
    public var items: [ListItemProtocol]

    public init(id: String, header: String?, items: [ListItemProtocol]) {
        self.id = id
        self.header = header
        self.items = items
    }
}
