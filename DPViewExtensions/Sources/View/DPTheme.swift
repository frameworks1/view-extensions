//
//  DPTheme.swift
//  DPViewExtensions
//
//  Created by Imthath M on 23/09/19.
//  Copyright © 2018 Imthath. All rights reserved.
//

import Foundation
import UIKit

public class DPTheme {
    internal static var light: DPThemeProtocol = DPLightTheme.shared
    internal static var dark: DPThemeProtocol = DPDarkTheme.shared

    internal static var style: DPUserInterfaceStyle = _mode
    internal static var current: DPThemeProtocol = light

    public static var isDark: Bool = false
}

public enum DPUserInterfaceStyle {
    case dark
    case light

    @available(iOS 13.0, *)
    case system
}

// MARK: - Theme handling
public extension DPTheme {

    static func updateLightTheme(using theme: DPThemeProtocol) {
        DPTheme.light = theme
    }

    static func updateDarkTheme(using theme: DPThemeProtocol) {
        DPTheme.dark = theme
    }

    /// Ensure that you have set UIApplication.shared.statusBarStyle appropriate for the theme you are setting here
    /// No action is required if you are choosing system theme
    static func setTheme(_ theme: DPUserInterfaceStyle) {
        DPTheme.style = theme
        switch theme {
        case .dark:
            DPTheme.isDark = true
            DPTheme.current = DPTheme.dark
        case .light:
            DPTheme.isDark = false
            DPTheme.current = DPTheme.light
        case .system:
            "Theme will be updated when view loads the next time".log()
        }
    }
}

extension DPTheme {

    private static var _mode: DPUserInterfaceStyle {
        get {
            if #available(iOS 13.0, *) {
                return .system
            }

            return .light
        }
    }

    @available(iOS 13.0, *)
    internal static func update(basedOn userInterfaceStyle: UIUserInterfaceStyle) {

        guard DPTheme.style == .system else { return }

        switch userInterfaceStyle {
        case .dark:
            isDark = true
            current = dark
        case .light, .unspecified:
            isDark = false
            current = light
        @unknown default:
            assertionFailure("Support not provided for new theme yet.")
        }

    }
}

public enum DPColor {
    case primaryText, secondaryText, placeholderText
    case primaryBG, secondaryBG, placeholderBG
    case border, interactive

    public var value: UIColor {
        switch self {
        case .primaryText:
            return DPTheme.current.primaryTextColor
        case .secondaryText:
            return DPTheme.current.secondaryTextColor
        case .placeholderText:
            return DPTheme.current.placeholderTextColor
        case .primaryBG:
            return DPTheme.current.primaryBackGroundColor ?? .clear
        case .secondaryBG:
            return DPTheme.current.secondaryBackGroundColor ?? .clear
        case .placeholderBG:
            return DPTheme.current.secondaryBackGroundColor ?? .clear
        case .border:
            return DPTheme.current.borderColor
        case .interactive:
            return DPTheme.current.interactiveColor
        }
    }
}

extension DPTheme {
    public static var primaryBackGroundColor: UIColor? { DPTheme.current.primaryBackGroundColor }
    public static var secondaryBackGroundColor: UIColor? { DPTheme.current.secondaryBackGroundColor }
    public static var placeholderBackGroundColor: UIColor? { DPTheme.current.placeholderBackGroundColor }

    public static var primaryTextColor: UIColor { DPTheme.current.primaryTextColor }
    public static var secondaryTextColor: UIColor { DPTheme.current.secondaryTextColor }
    public static var placeholderTextColor: UIColor { DPTheme.current.placeholderTextColor }

    public static var interactiveColor: UIColor { DPTheme.current.interactiveColor }
//    public static var nonInteractiveColor: UIColor { DPTheme.current.nonInteractiveColor }
    public static var borderColor: UIColor { DPTheme.current.borderColor }

    public static var successColor: UIColor { DPTheme.current.successColor }
    public static var failureColor: UIColor { DPTheme.current.failureColor }

    public static var navBar: DPNavigationBarThemeProtocol { DPTheme.current.navBarTheme }
}

@objc public protocol DPThemeProtocol {
    var primaryBackGroundColor: UIColor? { get }
    var secondaryBackGroundColor: UIColor? { get }
    var placeholderBackGroundColor: UIColor? { get }

    var primaryTextColor: UIColor { get }
    var secondaryTextColor: UIColor { get }
    var placeholderTextColor: UIColor { get }

    var borderColor: UIColor { get }
//    var selectionColor: UIColor { get }
    var interactiveColor: UIColor { get }
//    var nonInteractiveColor: UIColor { get }

    var successColor: UIColor { get }
    var failureColor: UIColor { get }

    var navBarTheme: DPNavigationBarThemeProtocol { get }
}

open class DPLightTheme: DPThemeProtocol {

    public init() { }

    internal static let shared = DPLightTheme()

    open var primaryBackGroundColor: UIColor? { .white }

    open var secondaryBackGroundColor: UIColor? { UIColor(red: 0.95, green: 0.95, blue: 0.96, alpha: 1.00) }

    open var placeholderBackGroundColor: UIColor? { return UIColor(rbg: 245) }

    open var primaryTextColor: UIColor { UIColor(red: 0.18, green: 0.24, blue: 0.30, alpha: 1.00) }

    open var secondaryTextColor: UIColor { return #colorLiteral(red: 0.3607843137, green: 0.3882352941, blue: 0.4078431373, alpha: 1) }

    open var placeholderTextColor: UIColor { return .lightGray}

    open var borderColor: UIColor { UIColor(red: 0.70, green: 0.70, blue: 0.70, alpha: 1.00) }

//    open var selectionColor: UIColor { return .lightGray }

    open var interactiveColor: UIColor { #colorLiteral(red: 1, green: 0.3843137255, blue: 0.3215686275, alpha: 1) }

//    open var nonInteractiveColor: UIColor { return UIColor(red: 213, green: 221, blue: 230) }

    open var successColor: UIColor { #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1) }

    open var failureColor: UIColor { #colorLiteral(red: 1, green: 0.3529411765, blue: 0.3725490196, alpha: 1) }

    open var navBarTheme: DPNavigationBarThemeProtocol = DPNavigationBarLightTheme.shared

}

open class DPDarkTheme: DPThemeProtocol {

    public init() { }

    internal static let shared = DPDarkTheme()

    open var primaryBackGroundColor: UIColor? { return UIColor(rbg: 0) }

    open var secondaryBackGroundColor: UIColor? { return UIColor(red: 22, green: 24, blue: 26) }

    open var placeholderBackGroundColor: UIColor? { return #colorLiteral(red: 0.0862745098, green: 0.09411764706, blue: 0.1019607843, alpha: 1) }

    open var primaryTextColor: UIColor { #colorLiteral(red: 0.7882352941, green: 0.8117647059, blue: 0.8470588235, alpha: 1) }

    open var secondaryTextColor: UIColor { return #colorLiteral(red: 0.4039215686, green: 0.4352941176, blue: 0.4745098039, alpha: 1) }

    open var placeholderTextColor: UIColor { return #colorLiteral(red: 0.2431372549, green: 0.262745098, blue: 0.2823529412, alpha: 1) }

    open var borderColor: UIColor { return #colorLiteral(red: 0.1137254902, green: 0.1254901961, blue: 0.1333333333, alpha: 1) }

//    open var selectionColor: UIColor { return .lightGray }

    open var interactiveColor: UIColor { return #colorLiteral(red: 0.2196078431, green: 0.4235294118, blue: 0.9843137255, alpha: 1) }

//    open var nonInteractiveColor: UIColor { return UIColor(red: 213, green: 221, blue: 230) }

    open var successColor: UIColor { #colorLiteral(red: 0.2235294118, green: 0.7098039216, blue: 0.2901960784, alpha: 1) }

    open var failureColor: UIColor { UIColor(red: 237, green: 56, blue: 58)}

    open var navBarTheme: DPNavigationBarThemeProtocol = DPNavigationBarDarkTheme.shared

}
