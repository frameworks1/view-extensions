//
//  Utilities.swift
//  DPViewExtensions
//
//  Created by Imthath M on 23/09/19.
//  Copyright © 2018 Imthath. All rights reserved.
//

import Foundation

extension Numeric {
    private func toNSNumber() -> NSNumber? {
        if let num = self as? NSNumber { return num }
        guard let string = self as? String, let double = Double(string) else { return nil }
        return NSNumber(value: double)
    }

    func precision(_ minimumFractionDigits: Int, minimumIntegerDigits: Int = 1,
                   roundingMode: NumberFormatter.RoundingMode = NumberFormatter.RoundingMode.halfUp) -> String {
        guard let number = toNSNumber() else { return "\(self)" }
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = minimumFractionDigits
        formatter.roundingMode = roundingMode
        formatter.minimumIntegerDigits = minimumIntegerDigits
        return formatter.string(from: number) ?? "\(self)"
    }
}

extension URL {

    internal var size: Double {
        if let attributes = try? FileManager.default.attributesOfItem(atPath: self.path),
            let size = attributes[FileAttributeKey.size] as? NSNumber {
            return size.doubleValue / Double(1024*1024)
        }

        return 0
    }
}

extension Double {

    func appendUnit() -> String {
        if self <= 1 {
            return String(format: "%.2f", arguments: [self * 1024]) + " KB"
        }

        return String(format: "%.2f", arguments: [self]) + " MB"
    }
}

public extension Date {
    
    var relativeTimeString: String? {
        if #available(iOS 13.0, *) {
            return relativeTime(in: .current)
        }
        
        return relativeTime(using: DPString.date)
    }

    @available(iOS 13.0, *)
    func relativeTime(in locale: Locale) -> String {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .full
        return formatter.localizedString(for: self, relativeTo: Date())
    }
    
    @available(iOS, introduced: 9.0, deprecated: 13.0)
    func relativeTime(using dateString: DPDateString ) -> String? {

        let now = Date()

        guard self < now else { return nil }

        let components = Calendar.current.dateComponents([.year, .month, .weekOfYear, .day, .hour, .minute, .second],
                                                         from: self, to: now)

        if let count = components.year, count > 0 {
            return count == 1 ? dateString.yearAgo : String(format: dateString.yearsAgo, "\(count)")
        }

        if let count = components.month, count > 0 {
            return count == 1 ? dateString.monthAgo : String(format: dateString.monthsAgo, "\(count)")
        }

        if let count = components.weekOfYear, count > 0 {
            return count == 1 ? dateString.weekAgo : String(format: dateString.weeksAgo, "\(count)")
        }

        if let count = components.day, count > 0 {
            return count == 1 ? dateString.yesterday : String(format: dateString.daysAgo, "\(count)")
        }

        if let count = components.hour, count > 0 {
            return count == 1 ? dateString.hourAgo : String(format: dateString.hoursAgo, "\(count)")
        }

        if let count = components.minute, count > 0 {
            return count == 1 ? dateString.minuteAgo : String(format: dateString.minutesAgo, "\(count)")
        }

        return dateString.justNow
    }
}

extension Int {

    public var isZero: Bool { return self == 0 }

    public func spellOut(singular: String, plural: String) -> String? {

        guard self > 0 else { return nil }

        switch self {
        case 1:
            return singular
        default:
            return String(format: plural, "\(self)")
        }
    }
}
