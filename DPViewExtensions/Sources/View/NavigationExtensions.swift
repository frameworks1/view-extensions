//
//  UIAppExtension.swift
//  DPViewExtensions
//
//  Created by Imthath M on 19/12/19.
//  Copyright © 2019 Imthath. All rights reserved.
//

import UIKit

//// MARK: Set Status bar color
//public extension UIApplication {
//
//    @available(iOS, introduced: 9.0, deprecated: 13.0)
//    var statusBarView: UIView? {
//        if responds(to: Selector(("statusBar"))) {
//            return value(forKey: "statusBar") as? UIView
//        }
//        return nil
//    }
//
//    func setStatusBarColor(_ color: UIColor?) {
//        if #available(iOS 13.0, *) {
//            addStatusBarView(withBG: color)
//        } else {
//             statusBarView?.backgroundColor = color
//        }
//    }
//
//    @available(iOS 13.0, *)
//    func addStatusBarView(withBG color: UIColor?) {
//        guard let statusBar = keyWindow?.subviews.compactMap({ $0 as? DPStatusBarView }).first else {
//            let statusBar = DPStatusBarView(backgroundColor: color)
//            keyWindow?.addSubview(statusBar)
//            return
//        }
//
//        statusBar.backgroundColor = color
//    }
//
//    func removeStatusBarColor() {
//        if #available(iOS 13.0, *),
//            let statusBar = keyWindow?.subviews.compactMap({ $0 as? DPStatusBarView }).first {
//            statusBar.removeFromSuperview()
//        } else {
//            statusBarView?.backgroundColor = .clear
//        }
//    }
//}
//
//@available(iOS 13, *)
//class DPStatusBarView: UIView {
//
//    init(backgroundColor: UIColor?) {
//        super.init(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
//        self.backgroundColor = backgroundColor
//    }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//}

// MARK: Static Helpers
public extension UIApplication {

    static var topController: UIViewController? {
        if var topController = shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }

            return topController
        }

        return nil
    }

    static func pushOrPresent(_ viewController: UIViewController,
                              as style: UIModalPresentationStyle = .fullScreen,
                              animated flag: Bool = true, withTitle title: String? = nil,
                              canCancel addCancel: Bool = true) {
        if let navController = topController as? UINavigationController {
            navController.pushViewController(viewController, animated: flag)
            viewController.navigationItem.title = title
//        } else if #available(iOS 11, *) {
//            presentWithLargeTitle(viewController, as: style, animated: flag, withTitle: title)
        } else {
            present(viewController, as: style, animated: flag, withTitle: title)
        }
    }

    static func present(_ viewController: UIViewController,
                        as style: UIModalPresentationStyle = .fullScreen,
                        animated flag: Bool = true, withTitle title: String? = nil,
                        onCompletion handler: (() -> Void)? = nil,
                        canCancel addCancel: Bool = true) {
        let navController = getNavController(viewController, as: style, animated: flag, withTitle: title, canCancel: addCancel)
        topController?.present(navController, animated: flag, completion: handler)
    }

    static func requestDeleteConfirmation(onCompletion handler: @escaping (Bool) -> Void) {
        requestConfirmation(withTitle: DPString.general.deleteAlertTitle, andMessage: DPString.general.delete, onCompletion: handler)
    }

    static func requestConfirmation(withTitle title: String, andMessage message: String, onCompletion handler: @escaping (Bool) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: DPString.general.yes, style: .destructive, handler:  { _ in
            handler(true)
        }))

        alert.addAction(UIAlertAction(title: DPString.general.no, style: .default, handler:  { _ in
            handler(false)
        }))

        topController?.present(alert, animated: true)
    }

//    @available(iOS 11.0, *)
//    static func presentWithLargeTitle(_ viewController: UIViewController,
//                                       as style: UIModalPresentationStyle = DPPresentationStyle,
//                                       animated flag: Bool = true, withTitle title: String? = nil,
//                                       onCompletion handler: (() -> Void)? = nil) {
//        let navController = getNavController(viewController, as: style, animated: flag, withTitle: title)
//        navController.navigationBar.prefersLargeTitles = true
//        viewController.navigationItem.largeTitleDisplayMode = .always
//        topController?.present(navController, animated: flag, completion: handler)
//    }

    fileprivate static func getNavController(_ viewController: UIViewController,
                                             as style: UIModalPresentationStyle = .fullScreen,
                                             animated flag: Bool = true,
                                             withTitle title: String? = nil,
                                             canCancel addCancel: Bool = true) -> UINavigationController {
        let navController = UINavigationController(rootViewController: viewController)
        navController.navigationBar.setValue(true, forKey: "hidesShadow")
        navController.modalPresentationStyle = style
        navController.setNavigationBarTheme(using: DPTheme.current.navBarTheme)
        viewController.navigationItem.title = title

        if addCancel {
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel,
                    target: viewController, action: #selector(viewController.defaultCancelAction))
        }

        return navController
    }

    @objc static func dismiss() {
        print(topController!)
        if let cancellableVC = topController as? Cancellable {
            UIAlertController.perfromExitCheck(on: cancellableVC)
        } else {
            topController?.dismiss(animated: true, completion: nil)
        }

    }

    /// Checks if view hierarchy of application contains `UIRemoteKeyboardWindow` if it does, keyboard is presented
    static var isKeyboardPresented: Bool {
        if let keyboardWindowClass = NSClassFromString("UIRemoteKeyboardWindow"),
            shared.windows.contains (where: { $0.isKind(of: keyboardWindowClass) }) {
            return true
        } else {
            return false
        }
    }
}

public extension UIAlertController {

    func addAction(withTitle title: String, completion: @escaping (UIAlertAction) -> Void) {
        self.addAction(UIAlertAction(title: title, style: .default, handler: completion))
    }

    @inlinable func addDismissAction(withTitle title: String) {
        self.addAction(UIAlertAction(title: title, style: .cancel, handler: nil))
    }

}

public extension UIViewController {

    // call this method when presenting any system provided view controllers like Image Picker, Document Picker
    func updateNativeTheme() {
        guard #available(iOS 13.0, *), DPTheme.style != .system else { return }

        if DPTheme.isDark {
            overrideUserInterfaceStyle = .dark
        } else {
            overrideUserInterfaceStyle = .light
        }
    }

    /// used while presenting from view controller.
    ///
    /// - Parameters:
    ///   - viewController: the viewcontroller to be embedded in a navigation controller and presented
    ///   - style: presentation style, default is what you have configured in DPPresentationStyle
    ///   - flag: can animate, default is ture
    ///   - title: title for the view controller
    ///   - handler: completion handler after presentation
    func show(_ viewController: UIViewController,
              as style: UIModalPresentationStyle = .fullScreen,
              animated flag: Bool = true, withTitle title: String? = nil,
              canCancel addCancel: Bool = true,
              onCompletion handler: (() -> Void)? = nil) {
        let navController = UIApplication.getNavController(viewController, as: style, animated: flag, withTitle: title, canCancel: addCancel)
        present(navController, animated: flag, completion: handler)
    }

    @inlinable func dismiss() {
        DispatchQueue.main.async {
            self.dismiss(animated: true)
        }
    }

    func showLoader() {
        view.makeToastActivity(.center, frame: view.frame)
    }

    func hideLoader() {
        view.hideToastActivity()
    }

    func showToast(withMessage message: String, forDuration seconds: Double = 2) {
        if let window = view.window {
            window.showToast(withMessage: message, forDuration: seconds)
        } else {
            view.showToast(withMessage: message, forDuration: seconds)
        }
    }
    
    /// call from ViewWillAppear where navigation controller will be present
    func enableLargeTitle() {
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    /// call from ViewWillAppear where navigation controller will be present
    func disableLargeTitle() {
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }
}

extension UIView {

    public func showToast(withMessage message: String, forDuration seconds: Double = 2) {
        if UIApplication.isKeyboardPresented {
            makeToast(message, position: .center)
        } else {
            makeToast(message, duration: seconds)
        }
    }
}
