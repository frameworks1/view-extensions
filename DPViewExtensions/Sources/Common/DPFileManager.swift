//
//  DPFileManager.swift
//  DPViewExtensions
//
//  Created by Imthath M on 08/11/19.
//  Copyright © 2019 Imthath M. All rights reserved.
//

import Foundation
import UIKit

let IMAGES_FOLDER = "Images"
let FILES_FOlDER = "Files"

class DPFileManager {

    static func save(_ image: UIImage, withName imageName: String, inFolder folderName: String = IMAGES_FOLDER) -> URL? {

        guard let data = image.jpegData(compressionQuality: 1) else { return nil }

        return save(data, withName: imageName, inFolder: folderName)
    }

    static func save(_ data: Data, withName fileName: String, inFolder folderName: String = FILES_FOlDER) -> URL? {

        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }

        let folderURL = documentsDirectory.appendingPathComponent(folderName, isDirectory: true)
        let fileURL = folderURL.appendingPathComponent(fileName)

        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            return fileURL
        }

        do {
            try FileManager.default.createDirectory(at: folderURL, withIntermediateDirectories: true, attributes: nil)
            try data.write(to: fileURL)
        } catch let error {
            "error saving file with error - \(error)".log()
        }

        return fileURL
    }

    static func getImage(withName fileName: String, inFolder folderName: String = IMAGES_FOLDER) -> UIImage? {

        guard let url = getURL(fileName: fileName, folderName: folderName) else { return nil }

        return UIImage(contentsOfFile: url.path)
    }

    private static func getURL(fileName: String, folderName: String) -> URL? {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory,
                                                                in: .userDomainMask).first else { return nil }

        return documentsDirectory.appendingPathComponent(folderName, isDirectory: true)
                                 .appendingPathComponent(fileName)
    }
}
