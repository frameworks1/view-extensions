//
//  DPAttachmentCell.swift
//  DPViewExtensions
//
//  Created by Imthath M on 16/10/19.
//  Copyright © 2019 Imthath M. All rights reserved.
//

import UIKit

public protocol DPAttachment: ListItemProtocol {
    var id: String { get }
    var name: String { get }
    var size: String { get }
    var url: String { get }
}

open class DPAttachmentCell: DPInteractiveCell {

    public static let identifier: String = "attachment"

    public var attachment: DPAttachment?

    public var isLastAttachment: Bool

    public var iconTitle: String?
    
    public lazy var icon = DPIconLabel(color: .secondaryText, size: DPIcon.smallSize)
    private lazy var name = DPLabel(color: .primaryText, font: DPFont.secondary)
    private lazy var size = DPLabel(color: .secondaryText, font: DPFont.footnote)
    private lazy var bottomSeparator = UILabel()

    convenience public init(_ object: DPAttachment, isLastCell: Bool = false) {
        self.init(style: .default, reuseIdentifier: DPAttachmentCell.identifier)
        self.attachment = object
        self.isLastAttachment = isLastCell
        configureViews()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        self.isLastAttachment = false
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setColors()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    open func alignIconLeading() {
        icon.align(.leading, with: contentView, offset: .sixteen)
    }
    
    override public func setColors() {
        super.setColors()
        backgroundColor = DPTheme.primaryBackGroundColor
        icon.setColors()
        name.setColors()
        size.setColors()
        bottomSeparator.backgroundColor = DPTheme.borderColor
    }
}

extension DPAttachmentCell {

    func configureViews() {
        guard let attachment = self.attachment else {
            assertionFailure("unable to init attachment cell")
            return
        }

        icon.text = iconTitle
        name.text = attachment.name
        size.text = attachment.size.byteSize

        setColors()
        layoutViews()
    }

    func layoutViews() {
        contentView.addSubview(icon)
        alignIconLeading()
        icon.alignVertically(with: contentView, offset: .sixteen)
        icon.make(.width, fraction: 0.1, as: contentView)

        contentView.addSubview(size)
        size.alignTrailing(with: contentView, offset: .sixteen)
        size.setContentHuggingPriority(.defaultHigh, for: .horizontal)
//        size.make(.width, fraction: 0.25, as: contentView)

        contentView.addSubview(name)
        name.alignVertically(with: contentView, offset: .sixteen)
        name.align(.leading, with: icon, on: .trailing, offset: .ten)
        name.align(.trailing, with: size, on: .leading, offset: -.ten)

        contentView.addSubview(bottomSeparator)
        bottomSeparator.align([.bottom, .trailing], with: contentView)
        bottomSeparator.fixHeight(.seperatorThickness)

        if isLastAttachment {
            bottomSeparator.align(.leading, with: contentView)
        } else {
            bottomSeparator.align(.leading, with: name)
        }
    }

//    override public func prepareForReuse() {
//        super.prepareForReuse()
//        isLastCell = false
//    }
}

extension DPAttachment {

    public func getCell(at indexPath: IndexPath, of tableView: UITableView) -> UITableViewCell {
        let isLastAttachment = indexPath.row == (tableView.numberOfRows(inSection: indexPath.section) - 1)

        return DPAttachmentCell(self, isLastCell: isLastAttachment)
    }
}
